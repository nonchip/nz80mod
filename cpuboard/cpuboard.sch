EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOTCONN1
U 1 1 5F91CCE1
P 1650 2350
F 0 "SLOTCONN1" H 1700 3667 50  0000 C CNN
F 1 "02x24 male 90deg" H 1700 3576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Horizontal" H 1650 2350 50  0001 C CNN
F 3 "~" H 1650 2350 50  0001 C CNN
	1    1650 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F921942
P 2150 900
F 0 "#PWR0101" H 2150 650 50  0001 C CNN
F 1 "GND" H 2155 727 50  0000 C CNN
F 2 "" H 2150 900 50  0001 C CNN
F 3 "" H 2150 900 50  0001 C CNN
	1    2150 900 
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F9209DC
P 1250 1000
F 0 "#PWR0102" H 1250 850 50  0001 C CNN
F 1 "+5V" H 1265 1173 50  0000 C CNN
F 2 "" H 1250 1000 50  0001 C CNN
F 3 "" H 1250 1000 50  0001 C CNN
	1    1250 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1000 1250 1250
Wire Wire Line
	2150 1250 1950 1250
Wire Wire Line
	1250 1250 1450 1250
Text Label 1450 1350 2    50   ~ 0
A0
Text Label 1450 1450 2    50   ~ 0
A1
Text Label 1450 1550 2    50   ~ 0
A2
Text Label 1450 1650 2    50   ~ 0
A3
Text Label 1450 1750 2    50   ~ 0
A4
Text Label 1450 1850 2    50   ~ 0
A5
Text Label 1450 1950 2    50   ~ 0
A6
Text Label 1450 2050 2    50   ~ 0
A7
Text Label 1450 2150 2    50   ~ 0
A8
Text Label 1450 2250 2    50   ~ 0
A9
Text Label 1450 2350 2    50   ~ 0
A10
Text Label 1450 2450 2    50   ~ 0
A11
Text Label 1450 2550 2    50   ~ 0
A12
Text Label 1450 2650 2    50   ~ 0
A13
Text Label 1450 2750 2    50   ~ 0
A14
Text Label 1450 2850 2    50   ~ 0
A15
Text Label 1950 1350 0    50   ~ 0
D0
Text Label 1950 1450 0    50   ~ 0
D1
Text Label 1950 1550 0    50   ~ 0
D2
Text Label 1950 1650 0    50   ~ 0
D3
Text Label 1950 1750 0    50   ~ 0
D4
Text Label 1950 1850 0    50   ~ 0
D5
Text Label 1950 1950 0    50   ~ 0
D6
Text Label 1950 2050 0    50   ~ 0
D7
Text Label 1950 2150 0    50   ~ 0
~M1
Text Label 1950 2250 0    50   ~ 0
~MREQ
Text Label 1950 2350 0    50   ~ 0
~IORQ
Text Label 1950 2450 0    50   ~ 0
~RD
Text Label 1950 2550 0    50   ~ 0
~WR
Text Label 1950 2650 0    50   ~ 0
~RFSH
Text Label 1950 2750 0    50   ~ 0
~HALT
Text Label 1950 2850 0    50   ~ 0
~WAIT
Text Label 1950 2950 0    50   ~ 0
~INT
Text Label 1950 3050 0    50   ~ 0
~NMI
Text Label 1950 3150 0    50   ~ 0
~RESET
Text Label 1950 3250 0    50   ~ 0
~BUSRQ
Text Label 1950 3350 0    50   ~ 0
~BUSACK
Text Label 1950 3450 0    50   ~ 0
~CLK
Text Label 1450 2950 2    50   ~ 0
IMEM
Text Label 1450 3050 2    50   ~ 0
ICLK
Text Label 1450 3150 2    50   ~ 0
USR0
Text Label 1450 3250 2    50   ~ 0
USR1
Text Label 1450 3350 2    50   ~ 0
USR2
Text Label 1450 3450 2    50   ~ 0
USR3
$Comp
L power:+5V #PWR0103
U 1 1 5F95CB71
P 1950 3650
F 0 "#PWR0103" H 1950 3500 50  0001 C CNN
F 1 "+5V" H 1965 3823 50  0000 C CNN
F 2 "" H 1950 3650 50  0001 C CNN
F 3 "" H 1950 3650 50  0001 C CNN
	1    1950 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 3550 1950 3650
$Comp
L symbols:ATF22V10 U1
U 1 1 5F9632F2
P 1400 4250
F 0 "U1" H 1725 4415 50  0000 C CNN
F 1 "ATF22V10" H 1725 4324 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_Socket" H 1400 4250 50  0001 C CNN
F 3 "" H 1400 4250 50  0001 C CNN
	1    1400 4250
	1    0    0    -1  
$EndComp
$Comp
L Oscillator:CXO_DIP8 X1
U 1 1 5F97F659
P 1700 6700
F 0 "X1" H 2044 6746 50  0000 L CNN
F 1 "CXO_DIP8" H 2044 6655 50  0000 L CNN
F 2 "Oscillator:Oscillator_DIP-8" H 2150 6350 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/OSZI.pdf" H 1600 6700 50  0001 C CNN
	1    1700 6700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F982344
P 2150 4200
F 0 "#PWR0104" H 2150 4050 50  0001 C CNN
F 1 "+5V" H 2165 4373 50  0000 C CNN
F 2 "" H 2150 4200 50  0001 C CNN
F 3 "" H 2150 4200 50  0001 C CNN
	1    2150 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5F982DDD
P 1300 5600
F 0 "#PWR0105" H 1300 5350 50  0001 C CNN
F 1 "GND" H 1305 5427 50  0000 C CNN
F 2 "" H 1300 5600 50  0001 C CNN
F 3 "" H 1300 5600 50  0001 C CNN
	1    1300 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F98335F
P 1700 7150
F 0 "#PWR0106" H 1700 6900 50  0001 C CNN
F 1 "GND" H 1705 6977 50  0000 C CNN
F 2 "" H 1700 7150 50  0001 C CNN
F 3 "" H 1700 7150 50  0001 C CNN
	1    1700 7150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5F983902
P 1700 6300
F 0 "#PWR0107" H 1700 6150 50  0001 C CNN
F 1 "+5V" H 1715 6473 50  0000 C CNN
F 2 "" H 1700 6300 50  0001 C CNN
F 3 "" H 1700 6300 50  0001 C CNN
	1    1700 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5450 1300 5600
Wire Wire Line
	2150 4200 2150 4350
Wire Wire Line
	1700 7000 1700 7150
$Comp
L Memory_EEPROM:28C256 U2
U 1 1 5F98C26E
P 4300 2150
F 0 "U2" H 4300 3431 50  0000 C CNN
F 1 "28C256" H 4300 3340 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 4300 2150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0006.pdf" H 4300 2150 50  0001 C CNN
	1    4300 2150
	1    0    0    -1  
$EndComp
Text Label 3900 1250 2    50   ~ 0
A0
Text Label 3900 1350 2    50   ~ 0
A1
Text Label 3900 1450 2    50   ~ 0
A2
Text Label 3900 1550 2    50   ~ 0
A3
Text Label 3900 1650 2    50   ~ 0
A4
Text Label 3900 1750 2    50   ~ 0
A5
Text Label 3900 1850 2    50   ~ 0
A6
Text Label 3900 1950 2    50   ~ 0
A7
Text Label 3900 2050 2    50   ~ 0
A8
Text Label 3900 2150 2    50   ~ 0
A9
Text Label 3900 2250 2    50   ~ 0
A10
Text Label 3900 2350 2    50   ~ 0
A11
Text Label 3900 2450 2    50   ~ 0
A12
Text Label 3900 2550 2    50   ~ 0
A13
Text Label 3900 2650 2    50   ~ 0
A14
Text Label 3900 2950 2    50   ~ 0
~RD
$Comp
L power:+5V #PWR0108
U 1 1 5F99D3A8
P 3650 2850
F 0 "#PWR0108" H 3650 2700 50  0001 C CNN
F 1 "+5V" V 3665 2978 50  0000 L CNN
F 2 "" H 3650 2850 50  0001 C CNN
F 3 "" H 3650 2850 50  0001 C CNN
	1    3650 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 2850 3900 2850
$Comp
L power:+5V #PWR0109
U 1 1 5F9A0FA5
P 4300 900
F 0 "#PWR0109" H 4300 750 50  0001 C CNN
F 1 "+5V" H 4315 1073 50  0000 C CNN
F 2 "" H 4300 900 50  0001 C CNN
F 3 "" H 4300 900 50  0001 C CNN
	1    4300 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 900  4300 1050
Text Label 4700 1250 0    50   ~ 0
D0
Text Label 4700 1350 0    50   ~ 0
D1
Text Label 4700 1450 0    50   ~ 0
D2
Text Label 4700 1550 0    50   ~ 0
D3
Text Label 4700 1650 0    50   ~ 0
D4
Text Label 4700 1750 0    50   ~ 0
D5
Text Label 4700 1850 0    50   ~ 0
D6
Text Label 4700 1950 0    50   ~ 0
D7
$Comp
L power:GND #PWR0110
U 1 1 5F9A9CC3
P 4300 3350
F 0 "#PWR0110" H 4300 3100 50  0001 C CNN
F 1 "GND" H 4305 3177 50  0000 C CNN
F 2 "" H 4300 3350 50  0001 C CNN
F 3 "" H 4300 3350 50  0001 C CNN
	1    4300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3350 4300 3250
$Comp
L Memory_RAM:HM62256BLP U3
U 1 1 5F9AD66E
P 6200 2150
F 0 "U3" H 6200 3231 50  0000 C CNN
F 1 "HM62256BLP" H 6200 3140 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 6200 2050 50  0001 C CNN
F 3 "https://web.mit.edu/6.115/www/document/62256.pdf" H 6200 2050 50  0001 C CNN
	1    6200 2150
	1    0    0    -1  
$EndComp
Text Label 5700 1450 2    50   ~ 0
A0
Text Label 5700 1550 2    50   ~ 0
A1
Text Label 5700 1650 2    50   ~ 0
A2
Text Label 5700 1750 2    50   ~ 0
A3
Text Label 5700 1850 2    50   ~ 0
A4
Text Label 5700 1950 2    50   ~ 0
A5
Text Label 5700 2050 2    50   ~ 0
A6
Text Label 5700 2150 2    50   ~ 0
A7
Text Label 5700 2250 2    50   ~ 0
A8
Text Label 5700 2350 2    50   ~ 0
A9
Text Label 5700 2450 2    50   ~ 0
A10
Text Label 5700 2550 2    50   ~ 0
A11
Text Label 5700 2650 2    50   ~ 0
A12
Text Label 5700 2750 2    50   ~ 0
A13
Text Label 5700 2850 2    50   ~ 0
A14
Text Label 6700 1450 0    50   ~ 0
D0
Text Label 6700 1550 0    50   ~ 0
D1
Text Label 6700 1650 0    50   ~ 0
D2
Text Label 6700 1750 0    50   ~ 0
D3
Text Label 6700 1850 0    50   ~ 0
D4
Text Label 6700 1950 0    50   ~ 0
D5
Text Label 6700 2050 0    50   ~ 0
D6
Text Label 6700 2150 0    50   ~ 0
D7
Wire Wire Line
	7100 2350 6700 2350
Text Label 6700 2550 0    50   ~ 0
~RD
Text Label 6700 2650 0    50   ~ 0
~WR
$Comp
L power:+5V #PWR0111
U 1 1 5F9BE0F6
P 6200 1050
F 0 "#PWR0111" H 6200 900 50  0001 C CNN
F 1 "+5V" H 6215 1223 50  0000 C CNN
F 2 "" H 6200 1050 50  0001 C CNN
F 3 "" H 6200 1050 50  0001 C CNN
	1    6200 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5F9BE82C
P 6200 3250
F 0 "#PWR0112" H 6200 3000 50  0001 C CNN
F 1 "GND" H 6205 3077 50  0000 C CNN
F 2 "" H 6200 3250 50  0001 C CNN
F 3 "" H 6200 3250 50  0001 C CNN
	1    6200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3250 6200 3050
Wire Wire Line
	6200 1050 6200 1250
$Comp
L CPU:Z80CPU U4
U 1 1 5F9D17E4
P 8600 2550
F 0 "U4" H 8600 4231 50  0000 C CNN
F 1 "Z80CPU" H 8600 4140 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm_Socket" H 8600 2950 50  0001 C CNN
F 3 "www.zilog.com/manage_directlink.php?filepath=docs/z80/um0080" H 8600 2950 50  0001 C CNN
	1    8600 2550
	1    0    0    -1  
$EndComp
Text Label 7900 1350 2    50   ~ 0
~RESET
Text Label 7900 1650 2    50   ~ 0
~CLK
Text Label 7900 1950 2    50   ~ 0
~NMI
Text Label 7900 2050 2    50   ~ 0
~INT
Text Label 7900 2350 2    50   ~ 0
~M1
Text Label 7900 2450 2    50   ~ 0
~RFSH
Text Label 7900 2550 2    50   ~ 0
~WAIT
Text Label 7900 2650 2    50   ~ 0
~HALT
Text Label 7900 3050 2    50   ~ 0
~RD
Text Label 7900 3150 2    50   ~ 0
~WR
Text Label 7900 3250 2    50   ~ 0
~MREQ
Text Label 7900 3350 2    50   ~ 0
~IORQ
Text Label 7900 3650 2    50   ~ 0
~BUSRQ
Text Label 7900 3750 2    50   ~ 0
~BUSACK
Text Label 9300 1350 0    50   ~ 0
A0
Text Label 9300 1450 0    50   ~ 0
A1
Text Label 9300 1550 0    50   ~ 0
A2
Text Label 9300 1650 0    50   ~ 0
A3
Text Label 9300 1750 0    50   ~ 0
A4
Text Label 9300 1850 0    50   ~ 0
A5
Text Label 9300 1950 0    50   ~ 0
A6
Text Label 9300 2050 0    50   ~ 0
A7
Text Label 9300 2150 0    50   ~ 0
A8
Text Label 9300 2250 0    50   ~ 0
A9
Text Label 9300 2350 0    50   ~ 0
A10
Text Label 9300 2450 0    50   ~ 0
A11
Text Label 9300 2550 0    50   ~ 0
A12
Text Label 9300 2650 0    50   ~ 0
A13
Text Label 9300 2750 0    50   ~ 0
A14
Text Label 9300 2850 0    50   ~ 0
A15
Text Label 9300 3050 0    50   ~ 0
D0
Text Label 9300 3150 0    50   ~ 0
D1
Text Label 9300 3250 0    50   ~ 0
D2
Text Label 9300 3350 0    50   ~ 0
D3
Text Label 9300 3450 0    50   ~ 0
D4
Text Label 9300 3550 0    50   ~ 0
D5
Text Label 9300 3650 0    50   ~ 0
D6
Text Label 9300 3750 0    50   ~ 0
D7
$Comp
L power:GND #PWR0113
U 1 1 5F9F15AB
P 8600 4150
F 0 "#PWR0113" H 8600 3900 50  0001 C CNN
F 1 "GND" H 8605 3977 50  0000 C CNN
F 2 "" H 8600 4150 50  0001 C CNN
F 3 "" H 8600 4150 50  0001 C CNN
	1    8600 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 5F9F1C09
P 8600 900
F 0 "#PWR0114" H 8600 750 50  0001 C CNN
F 1 "+5V" H 8615 1073 50  0000 C CNN
F 2 "" H 8600 900 50  0001 C CNN
F 3 "" H 8600 900 50  0001 C CNN
	1    8600 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 900  8600 1050
Wire Wire Line
	8600 4050 8600 4150
$Comp
L Device:R_Network08 RN1
U 1 1 5FA0E361
P 2800 3000
F 0 "RN1" V 2183 3000 50  0000 C CNN
F 1 "8x10K" V 2274 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 3275 3000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 2800 3000 50  0001 C CNN
	1    2800 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 3050 1200 3050
Wire Wire Line
	2400 3850 2400 3200
Wire Wire Line
	2400 3200 2600 3200
Wire Wire Line
	1450 2950 1100 2950
Wire Wire Line
	2450 3900 2450 3300
Wire Wire Line
	2450 3300 2600 3300
Wire Wire Line
	2600 3100 2350 3100
Wire Wire Line
	2350 3100 2350 3250
Wire Wire Line
	2350 3250 1950 3250
Wire Wire Line
	2300 3000 2300 3150
Wire Wire Line
	2300 3150 1950 3150
Wire Wire Line
	2600 2900 2250 2900
Wire Wire Line
	2250 2900 2250 3050
Wire Wire Line
	2250 3050 1950 3050
Wire Wire Line
	2200 2950 1950 2950
Wire Wire Line
	2600 2800 2200 2800
Wire Wire Line
	2200 2800 2200 2950
Wire Wire Line
	2600 2700 2150 2700
Wire Wire Line
	2150 2700 2150 2850
Wire Wire Line
	2150 2850 1950 2850
NoConn ~ 1450 3550
Wire Wire Line
	2150 900  2150 1250
$Comp
L power:+5V #PWR0115
U 1 1 5FA3D540
P 3000 2400
F 0 "#PWR0115" H 3000 2250 50  0001 C CNN
F 1 "+5V" H 3015 2573 50  0000 C CNN
F 2 "" H 3000 2400 50  0001 C CNN
F 3 "" H 3000 2400 50  0001 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2400 3000 2600
$Comp
L Switch:SW_Push SW1
U 1 1 5FAFF3ED
P 3750 6950
F 0 "SW1" V 3704 7098 50  0000 L CNN
F 1 "RESET" V 3795 7098 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3750 7150 50  0001 C CNN
F 3 "~" H 3750 7150 50  0001 C CNN
	1    3750 6950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5FB08027
P 3750 7150
F 0 "#PWR0118" H 3750 6900 50  0001 C CNN
F 1 "GND" H 3755 6977 50  0000 C CNN
F 2 "" H 3750 7150 50  0001 C CNN
F 3 "" H 3750 7150 50  0001 C CNN
	1    3750 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3850 1100 2950
Wire Wire Line
	1100 3850 2400 3850
Wire Wire Line
	1200 3050 1200 3900
Wire Wire Line
	1200 3900 2450 3900
Text Label 1300 4550 2    50   ~ 0
~MREQ
Text Label 1300 4650 2    50   ~ 0
A15
Text Label 1300 4450 2    50   ~ 0
IMEM
Wire Wire Line
	2350 6700 2000 6700
Text Label 2150 5350 0    50   ~ 0
~CLK
Wire Wire Line
	1700 6350 1700 6400
Wire Wire Line
	1700 6300 1700 6350
Connection ~ 1700 6350
Wire Wire Line
	1400 6350 1700 6350
Wire Wire Line
	1400 6700 1400 6350
Text Label 1300 4350 2    50   ~ 0
ICLK
$Comp
L power:+5V #PWR0119
U 1 1 5FCCEE24
P 3700 5750
F 0 "#PWR0119" H 3700 5600 50  0001 C CNN
F 1 "+5V" H 3715 5923 50  0000 C CNN
F 2 "" H 3700 5750 50  0001 C CNN
F 3 "" H 3700 5750 50  0001 C CNN
	1    3700 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5FCD1A78
P 3700 6050
F 0 "#PWR0120" H 3700 5800 50  0001 C CNN
F 1 "GND" H 3705 5877 50  0000 C CNN
F 2 "" H 3700 6050 50  0001 C CNN
F 3 "" H 3700 6050 50  0001 C CNN
	1    3700 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FCD8193
P 4000 5900
F 0 "C1" H 4115 5946 50  0000 L CNN
F 1 "C" H 4115 5855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4038 5750 50  0001 C CNN
F 3 "~" H 4000 5900 50  0001 C CNN
	1    4000 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FCD89CB
P 4300 5900
F 0 "C2" H 4415 5946 50  0000 L CNN
F 1 "C" H 4415 5855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4338 5750 50  0001 C CNN
F 3 "~" H 4300 5900 50  0001 C CNN
	1    4300 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FCD9093
P 4550 5900
F 0 "C3" H 4665 5946 50  0000 L CNN
F 1 "C" H 4665 5855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4588 5750 50  0001 C CNN
F 3 "~" H 4550 5900 50  0001 C CNN
	1    4550 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FCD96E1
P 4850 5900
F 0 "C4" H 4965 5946 50  0000 L CNN
F 1 "C" H 4965 5855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4888 5750 50  0001 C CNN
F 3 "~" H 4850 5900 50  0001 C CNN
	1    4850 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5750 4000 5750
Wire Wire Line
	4000 5750 4300 5750
Connection ~ 4000 5750
Wire Wire Line
	4300 5750 4550 5750
Connection ~ 4300 5750
Wire Wire Line
	4550 5750 4850 5750
Connection ~ 4550 5750
Wire Wire Line
	4850 6050 4550 6050
Wire Wire Line
	4550 6050 4300 6050
Connection ~ 4550 6050
Wire Wire Line
	4300 6050 4000 6050
Connection ~ 4300 6050
Wire Wire Line
	3700 6050 4000 6050
Connection ~ 4000 6050
Wire Wire Line
	2350 5450 2150 5450
Wire Wire Line
	2350 5450 2350 6700
Wire Wire Line
	2150 5150 3900 5150
Wire Wire Line
	3900 3050 3900 5150
Wire Wire Line
	2150 5250 7100 5250
Wire Wire Line
	7100 2350 7100 5250
Text Label 2150 4750 0    50   ~ 0
USR0
Text Label 2150 4650 0    50   ~ 0
USR1
Text Label 2150 4550 0    50   ~ 0
USR2
Text Label 2150 4450 0    50   ~ 0
USR3
Text Label 1300 4750 2    50   ~ 0
~IORQ
Text Label 1300 4850 2    50   ~ 0
~M1
Text Label 1300 4950 2    50   ~ 0
~RD
Text Label 1300 5050 2    50   ~ 0
~WR
Text Label 2150 4850 0    50   ~ 0
~WAIT
Text Label 2150 4950 0    50   ~ 0
~INT
$Comp
L Switch:SW_Push SW2
U 1 1 5FF93D38
P 4600 6950
F 0 "SW2" V 4554 7098 50  0000 L CNN
F 1 "NMI" V 4645 7098 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4600 7150 50  0001 C CNN
F 3 "~" H 4600 7150 50  0001 C CNN
	1    4600 6950
	0    1    1    0   
$EndComp
Text Label 4600 6750 0    50   ~ 0
~NMI
Wire Wire Line
	2300 3000 2600 3000
Text Label 3750 6750 0    50   ~ 0
~RESET
$Comp
L power:GND #PWR0116
U 1 1 5FFB3A8D
P 4600 7150
F 0 "#PWR0116" H 4600 6900 50  0001 C CNN
F 1 "GND" H 4605 6977 50  0000 C CNN
F 2 "" H 4600 7150 50  0001 C CNN
F 3 "" H 4600 7150 50  0001 C CNN
	1    4600 7150
	1    0    0    -1  
$EndComp
Text Label 1300 5150 2    50   ~ 0
~RFSH
NoConn ~ 2150 5050
Text Label 1300 5250 2    50   ~ 0
~BUSACK
Text Label 1300 5350 2    50   ~ 0
~HALT
$EndSCHEMATC
