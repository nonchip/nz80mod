EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOT0
U 1 1 5F91CCE1
P 3700 2500
F 0 "SLOT0" H 3750 3817 50  0000 C CNN
F 1 "02x24 female" H 3750 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x24_P2.54mm_Vertical" H 3700 2500 50  0001 C CNN
F 3 "~" H 3700 2500 50  0001 C CNN
	1    3700 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F921942
P 4200 1150
F 0 "#PWR0101" H 4200 900 50  0001 C CNN
F 1 "GND" H 4205 977 50  0000 C CNN
F 2 "" H 4200 1150 50  0001 C CNN
F 3 "" H 4200 1150 50  0001 C CNN
	1    4200 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F9209DC
P 3300 1150
F 0 "#PWR0102" H 3300 1000 50  0001 C CNN
F 1 "+5V" H 3315 1323 50  0000 C CNN
F 2 "" H 3300 1150 50  0001 C CNN
F 3 "" H 3300 1150 50  0001 C CNN
	1    3300 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1150 3300 1400
Wire Wire Line
	4200 1150 4200 1400
Wire Wire Line
	4200 1400 4000 1400
Wire Wire Line
	3300 1400 3500 1400
Text Label 3500 1500 2    50   ~ 0
A0
Text Label 3500 1600 2    50   ~ 0
A1
Text Label 3500 1700 2    50   ~ 0
A2
Text Label 3500 1800 2    50   ~ 0
A3
Text Label 3500 1900 2    50   ~ 0
A4
Text Label 3500 2000 2    50   ~ 0
A5
Text Label 3500 2100 2    50   ~ 0
A6
Text Label 3500 2200 2    50   ~ 0
A7
Text Label 3500 2300 2    50   ~ 0
A8
Text Label 3500 2400 2    50   ~ 0
A9
Text Label 3500 2500 2    50   ~ 0
A10
Text Label 3500 2600 2    50   ~ 0
A11
Text Label 3500 2700 2    50   ~ 0
A12
Text Label 3500 2800 2    50   ~ 0
A13
Text Label 3500 2900 2    50   ~ 0
A14
Text Label 3500 3000 2    50   ~ 0
A15
Text Label 4000 1500 0    50   ~ 0
D0
Text Label 4000 1600 0    50   ~ 0
D1
Text Label 4000 1700 0    50   ~ 0
D2
Text Label 4000 1800 0    50   ~ 0
D3
Text Label 4000 1900 0    50   ~ 0
D4
Text Label 4000 2000 0    50   ~ 0
D5
Text Label 4000 2100 0    50   ~ 0
D6
Text Label 4000 2200 0    50   ~ 0
D7
Text Label 4000 2300 0    50   ~ 0
~M1
Text Label 4000 2400 0    50   ~ 0
~MREQ
Text Label 4000 2500 0    50   ~ 0
~IORQ
Text Label 4000 2600 0    50   ~ 0
~RD
Text Label 4000 2700 0    50   ~ 0
~WR
Text Label 4000 2800 0    50   ~ 0
~RFSH
Text Label 4000 2900 0    50   ~ 0
~HALT
Text Label 4000 3000 0    50   ~ 0
~WAIT
Text Label 4000 3100 0    50   ~ 0
~INT
Text Label 4000 3200 0    50   ~ 0
~NMI
Text Label 4000 3300 0    50   ~ 0
~RESET
Text Label 4000 3400 0    50   ~ 0
~BUSRQ
Text Label 4000 3500 0    50   ~ 0
~BUSACK
Text Label 4000 3600 0    50   ~ 0
~CLK
Text Label 3500 3100 2    50   ~ 0
IMEM
Text Label 3500 3200 2    50   ~ 0
ICLK
Text Label 3500 3300 2    50   ~ 0
USR0
Text Label 3500 3400 2    50   ~ 0
USR1
Text Label 3500 3500 2    50   ~ 0
USR2
Text Label 3500 3600 2    50   ~ 0
USR3
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOT1
U 1 1 5F941339
P 4900 2500
F 0 "SLOT1" H 4950 3817 50  0000 C CNN
F 1 "02x24 female" H 4950 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x24_P2.54mm_Vertical" H 4900 2500 50  0001 C CNN
F 3 "~" H 4900 2500 50  0001 C CNN
	1    4900 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5F94133F
P 5400 1150
F 0 "#PWR0103" H 5400 900 50  0001 C CNN
F 1 "GND" H 5405 977 50  0000 C CNN
F 2 "" H 5400 1150 50  0001 C CNN
F 3 "" H 5400 1150 50  0001 C CNN
	1    5400 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F941345
P 4500 1150
F 0 "#PWR0104" H 4500 1000 50  0001 C CNN
F 1 "+5V" H 4515 1323 50  0000 C CNN
F 2 "" H 4500 1150 50  0001 C CNN
F 3 "" H 4500 1150 50  0001 C CNN
	1    4500 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1150 4500 1400
Wire Wire Line
	5400 1150 5400 1400
Wire Wire Line
	5400 1400 5200 1400
Wire Wire Line
	4500 1400 4700 1400
Text Label 4700 1500 2    50   ~ 0
A0
Text Label 4700 1600 2    50   ~ 0
A1
Text Label 4700 1700 2    50   ~ 0
A2
Text Label 4700 1800 2    50   ~ 0
A3
Text Label 4700 1900 2    50   ~ 0
A4
Text Label 4700 2000 2    50   ~ 0
A5
Text Label 4700 2100 2    50   ~ 0
A6
Text Label 4700 2200 2    50   ~ 0
A7
Text Label 4700 2300 2    50   ~ 0
A8
Text Label 4700 2400 2    50   ~ 0
A9
Text Label 4700 2500 2    50   ~ 0
A10
Text Label 4700 2600 2    50   ~ 0
A11
Text Label 4700 2700 2    50   ~ 0
A12
Text Label 4700 2800 2    50   ~ 0
A13
Text Label 4700 2900 2    50   ~ 0
A14
Text Label 4700 3000 2    50   ~ 0
A15
Text Label 5200 1500 0    50   ~ 0
D0
Text Label 5200 1600 0    50   ~ 0
D1
Text Label 5200 1700 0    50   ~ 0
D2
Text Label 5200 1800 0    50   ~ 0
D3
Text Label 5200 1900 0    50   ~ 0
D4
Text Label 5200 2000 0    50   ~ 0
D5
Text Label 5200 2100 0    50   ~ 0
D6
Text Label 5200 2200 0    50   ~ 0
D7
Text Label 5200 2300 0    50   ~ 0
~M1
Text Label 5200 2400 0    50   ~ 0
~MREQ
Text Label 5200 2500 0    50   ~ 0
~IORQ
Text Label 5200 2600 0    50   ~ 0
~RD
Text Label 5200 2700 0    50   ~ 0
~WR
Text Label 5200 2800 0    50   ~ 0
~RFSH
Text Label 5200 2900 0    50   ~ 0
~HALT
Text Label 5200 3000 0    50   ~ 0
~WAIT
Text Label 5200 3100 0    50   ~ 0
~INT
Text Label 5200 3200 0    50   ~ 0
~NMI
Text Label 5200 3300 0    50   ~ 0
~RESET
Text Label 5200 3400 0    50   ~ 0
~BUSRQ
Text Label 5200 3500 0    50   ~ 0
~BUSACK
Text Label 5200 3600 0    50   ~ 0
~CLK
Text Label 4700 3100 2    50   ~ 0
IMEM
Text Label 4700 3200 2    50   ~ 0
ICLK
Text Label 4700 3300 2    50   ~ 0
USR0
Text Label 4700 3400 2    50   ~ 0
USR1
Text Label 4700 3500 2    50   ~ 0
USR2
Text Label 4700 3600 2    50   ~ 0
USR3
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOT2
U 1 1 5F942654
P 6050 2500
F 0 "SLOT2" H 6100 3817 50  0000 C CNN
F 1 "02x24 female" H 6100 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x24_P2.54mm_Vertical" H 6050 2500 50  0001 C CNN
F 3 "~" H 6050 2500 50  0001 C CNN
	1    6050 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5F94265A
P 6550 1150
F 0 "#PWR0105" H 6550 900 50  0001 C CNN
F 1 "GND" H 6555 977 50  0000 C CNN
F 2 "" H 6550 1150 50  0001 C CNN
F 3 "" H 6550 1150 50  0001 C CNN
	1    6550 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5F942660
P 5650 1150
F 0 "#PWR0106" H 5650 1000 50  0001 C CNN
F 1 "+5V" H 5665 1323 50  0000 C CNN
F 2 "" H 5650 1150 50  0001 C CNN
F 3 "" H 5650 1150 50  0001 C CNN
	1    5650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1150 5650 1400
Wire Wire Line
	6550 1150 6550 1400
Wire Wire Line
	6550 1400 6350 1400
Wire Wire Line
	5650 1400 5850 1400
Text Label 5850 1500 2    50   ~ 0
A0
Text Label 5850 1600 2    50   ~ 0
A1
Text Label 5850 1700 2    50   ~ 0
A2
Text Label 5850 1800 2    50   ~ 0
A3
Text Label 5850 1900 2    50   ~ 0
A4
Text Label 5850 2000 2    50   ~ 0
A5
Text Label 5850 2100 2    50   ~ 0
A6
Text Label 5850 2200 2    50   ~ 0
A7
Text Label 5850 2300 2    50   ~ 0
A8
Text Label 5850 2400 2    50   ~ 0
A9
Text Label 5850 2500 2    50   ~ 0
A10
Text Label 5850 2600 2    50   ~ 0
A11
Text Label 5850 2700 2    50   ~ 0
A12
Text Label 5850 2800 2    50   ~ 0
A13
Text Label 5850 2900 2    50   ~ 0
A14
Text Label 5850 3000 2    50   ~ 0
A15
Text Label 6350 1500 0    50   ~ 0
D0
Text Label 6350 1600 0    50   ~ 0
D1
Text Label 6350 1700 0    50   ~ 0
D2
Text Label 6350 1800 0    50   ~ 0
D3
Text Label 6350 1900 0    50   ~ 0
D4
Text Label 6350 2000 0    50   ~ 0
D5
Text Label 6350 2100 0    50   ~ 0
D6
Text Label 6350 2200 0    50   ~ 0
D7
Text Label 6350 2300 0    50   ~ 0
~M1
Text Label 6350 2400 0    50   ~ 0
~MREQ
Text Label 6350 2500 0    50   ~ 0
~IORQ
Text Label 6350 2600 0    50   ~ 0
~RD
Text Label 6350 2700 0    50   ~ 0
~WR
Text Label 6350 2800 0    50   ~ 0
~RFSH
Text Label 6350 2900 0    50   ~ 0
~HALT
Text Label 6350 3000 0    50   ~ 0
~WAIT
Text Label 6350 3100 0    50   ~ 0
~INT
Text Label 6350 3200 0    50   ~ 0
~NMI
Text Label 6350 3300 0    50   ~ 0
~RESET
Text Label 6350 3400 0    50   ~ 0
~BUSRQ
Text Label 6350 3500 0    50   ~ 0
~BUSACK
Text Label 6350 3600 0    50   ~ 0
~CLK
Text Label 5850 3100 2    50   ~ 0
IMEM
Text Label 5850 3200 2    50   ~ 0
ICLK
Text Label 5850 3300 2    50   ~ 0
USR0
Text Label 5850 3400 2    50   ~ 0
USR1
Text Label 5850 3500 2    50   ~ 0
USR2
Text Label 5850 3600 2    50   ~ 0
USR3
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOT3
U 1 1 5F946470
P 7150 2500
F 0 "SLOT3" H 7200 3817 50  0000 C CNN
F 1 "02x24 female" H 7200 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x24_P2.54mm_Vertical" H 7150 2500 50  0001 C CNN
F 3 "~" H 7150 2500 50  0001 C CNN
	1    7150 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F946476
P 7650 1150
F 0 "#PWR0107" H 7650 900 50  0001 C CNN
F 1 "GND" H 7655 977 50  0000 C CNN
F 2 "" H 7650 1150 50  0001 C CNN
F 3 "" H 7650 1150 50  0001 C CNN
	1    7650 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0108
U 1 1 5F94647C
P 6750 1150
F 0 "#PWR0108" H 6750 1000 50  0001 C CNN
F 1 "+5V" H 6765 1323 50  0000 C CNN
F 2 "" H 6750 1150 50  0001 C CNN
F 3 "" H 6750 1150 50  0001 C CNN
	1    6750 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1150 6750 1400
Wire Wire Line
	7650 1150 7650 1400
Wire Wire Line
	7650 1400 7450 1400
Wire Wire Line
	6750 1400 6950 1400
Text Label 6950 1500 2    50   ~ 0
A0
Text Label 6950 1600 2    50   ~ 0
A1
Text Label 6950 1700 2    50   ~ 0
A2
Text Label 6950 1800 2    50   ~ 0
A3
Text Label 6950 1900 2    50   ~ 0
A4
Text Label 6950 2000 2    50   ~ 0
A5
Text Label 6950 2100 2    50   ~ 0
A6
Text Label 6950 2200 2    50   ~ 0
A7
Text Label 6950 2300 2    50   ~ 0
A8
Text Label 6950 2400 2    50   ~ 0
A9
Text Label 6950 2500 2    50   ~ 0
A10
Text Label 6950 2600 2    50   ~ 0
A11
Text Label 6950 2700 2    50   ~ 0
A12
Text Label 6950 2800 2    50   ~ 0
A13
Text Label 6950 2900 2    50   ~ 0
A14
Text Label 6950 3000 2    50   ~ 0
A15
Text Label 7450 1500 0    50   ~ 0
D0
Text Label 7450 1600 0    50   ~ 0
D1
Text Label 7450 1700 0    50   ~ 0
D2
Text Label 7450 1800 0    50   ~ 0
D3
Text Label 7450 1900 0    50   ~ 0
D4
Text Label 7450 2000 0    50   ~ 0
D5
Text Label 7450 2100 0    50   ~ 0
D6
Text Label 7450 2200 0    50   ~ 0
D7
Text Label 7450 2300 0    50   ~ 0
~M1
Text Label 7450 2400 0    50   ~ 0
~MREQ
Text Label 7450 2500 0    50   ~ 0
~IORQ
Text Label 7450 2600 0    50   ~ 0
~RD
Text Label 7450 2700 0    50   ~ 0
~WR
Text Label 7450 2800 0    50   ~ 0
~RFSH
Text Label 7450 2900 0    50   ~ 0
~HALT
Text Label 7450 3000 0    50   ~ 0
~WAIT
Text Label 7450 3100 0    50   ~ 0
~INT
Text Label 7450 3200 0    50   ~ 0
~NMI
Text Label 7450 3300 0    50   ~ 0
~RESET
Text Label 7450 3400 0    50   ~ 0
~BUSRQ
Text Label 7450 3500 0    50   ~ 0
~BUSACK
Text Label 7450 3600 0    50   ~ 0
~CLK
Text Label 6950 3100 2    50   ~ 0
IMEM
Text Label 6950 3200 2    50   ~ 0
ICLK
Text Label 6950 3300 2    50   ~ 0
USR0
Text Label 6950 3400 2    50   ~ 0
USR1
Text Label 6950 3500 2    50   ~ 0
USR2
Text Label 6950 3600 2    50   ~ 0
USR3
$Comp
L Connector_Generic:Conn_02x24_Odd_Even ENDIN1
U 1 1 5F94BD12
P 2100 2500
F 0 "ENDIN1" H 2150 3817 50  0000 C CNN
F 1 "02x24 male 90deg" H 2150 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Horizontal" H 2100 2500 50  0001 C CNN
F 3 "~" H 2100 2500 50  0001 C CNN
	1    2100 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5F94BD18
P 2600 1150
F 0 "#PWR0109" H 2600 900 50  0001 C CNN
F 1 "GND" H 2605 977 50  0000 C CNN
F 2 "" H 2600 1150 50  0001 C CNN
F 3 "" H 2600 1150 50  0001 C CNN
	1    2600 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5F94BD1E
P 1700 1150
F 0 "#PWR0110" H 1700 1000 50  0001 C CNN
F 1 "+5V" H 1715 1323 50  0000 C CNN
F 2 "" H 1700 1150 50  0001 C CNN
F 3 "" H 1700 1150 50  0001 C CNN
	1    1700 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1150 1700 1400
Wire Wire Line
	2600 1150 2600 1400
Wire Wire Line
	2600 1400 2400 1400
Wire Wire Line
	1700 1400 1900 1400
Text Label 1900 1500 2    50   ~ 0
A0
Text Label 1900 1600 2    50   ~ 0
A1
Text Label 1900 1700 2    50   ~ 0
A2
Text Label 1900 1800 2    50   ~ 0
A3
Text Label 1900 1900 2    50   ~ 0
A4
Text Label 1900 2000 2    50   ~ 0
A5
Text Label 1900 2100 2    50   ~ 0
A6
Text Label 1900 2200 2    50   ~ 0
A7
Text Label 1900 2300 2    50   ~ 0
A8
Text Label 1900 2400 2    50   ~ 0
A9
Text Label 1900 2500 2    50   ~ 0
A10
Text Label 1900 2600 2    50   ~ 0
A11
Text Label 1900 2700 2    50   ~ 0
A12
Text Label 1900 2800 2    50   ~ 0
A13
Text Label 1900 2900 2    50   ~ 0
A14
Text Label 1900 3000 2    50   ~ 0
A15
Text Label 2400 1500 0    50   ~ 0
D0
Text Label 2400 1600 0    50   ~ 0
D1
Text Label 2400 1700 0    50   ~ 0
D2
Text Label 2400 1800 0    50   ~ 0
D3
Text Label 2400 1900 0    50   ~ 0
D4
Text Label 2400 2000 0    50   ~ 0
D5
Text Label 2400 2100 0    50   ~ 0
D6
Text Label 2400 2200 0    50   ~ 0
D7
Text Label 2400 2300 0    50   ~ 0
~M1
Text Label 2400 2400 0    50   ~ 0
~MREQ
Text Label 2400 2500 0    50   ~ 0
~IORQ
Text Label 2400 2600 0    50   ~ 0
~RD
Text Label 2400 2700 0    50   ~ 0
~WR
Text Label 2400 2800 0    50   ~ 0
~RFSH
Text Label 2400 2900 0    50   ~ 0
~HALT
Text Label 2400 3000 0    50   ~ 0
~WAIT
Text Label 2400 3100 0    50   ~ 0
~INT
Text Label 2400 3200 0    50   ~ 0
~NMI
Text Label 2400 3300 0    50   ~ 0
~RESET
Text Label 2400 3400 0    50   ~ 0
~BUSRQ
Text Label 2400 3500 0    50   ~ 0
~BUSACK
Text Label 2400 3600 0    50   ~ 0
~CLK
Text Label 1900 3100 2    50   ~ 0
IMEM
Text Label 1900 3200 2    50   ~ 0
ICLK
Text Label 1900 3300 2    50   ~ 0
USR0
Text Label 1900 3400 2    50   ~ 0
USR1
Text Label 1900 3500 2    50   ~ 0
USR2
Text Label 1900 3600 2    50   ~ 0
USR3
$Comp
L power:GND #PWR0111
U 1 1 5F94EC69
P 9100 1150
F 0 "#PWR0111" H 9100 900 50  0001 C CNN
F 1 "GND" H 9105 977 50  0000 C CNN
F 2 "" H 9100 1150 50  0001 C CNN
F 3 "" H 9100 1150 50  0001 C CNN
	1    9100 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5F94EC6F
P 8200 1150
F 0 "#PWR0112" H 8200 1000 50  0001 C CNN
F 1 "+5V" H 8215 1323 50  0000 C CNN
F 2 "" H 8200 1150 50  0001 C CNN
F 3 "" H 8200 1150 50  0001 C CNN
	1    8200 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 1150 8200 1400
Wire Wire Line
	9100 1150 9100 1400
Wire Wire Line
	9100 1400 8900 1400
Wire Wire Line
	8200 1400 8400 1400
Text Label 8400 1500 2    50   ~ 0
A0
Text Label 8400 1600 2    50   ~ 0
A1
Text Label 8400 1700 2    50   ~ 0
A2
Text Label 8400 1800 2    50   ~ 0
A3
Text Label 8400 1900 2    50   ~ 0
A4
Text Label 8400 2000 2    50   ~ 0
A5
Text Label 8400 2100 2    50   ~ 0
A6
Text Label 8400 2200 2    50   ~ 0
A7
Text Label 8400 2300 2    50   ~ 0
A8
Text Label 8400 2400 2    50   ~ 0
A9
Text Label 8400 2500 2    50   ~ 0
A10
Text Label 8400 2600 2    50   ~ 0
A11
Text Label 8400 2700 2    50   ~ 0
A12
Text Label 8400 2800 2    50   ~ 0
A13
Text Label 8400 2900 2    50   ~ 0
A14
Text Label 8400 3000 2    50   ~ 0
A15
Text Label 8900 1500 0    50   ~ 0
D0
Text Label 8900 1600 0    50   ~ 0
D1
Text Label 8900 1700 0    50   ~ 0
D2
Text Label 8900 1800 0    50   ~ 0
D3
Text Label 8900 1900 0    50   ~ 0
D4
Text Label 8900 2000 0    50   ~ 0
D5
Text Label 8900 2100 0    50   ~ 0
D6
Text Label 8900 2200 0    50   ~ 0
D7
Text Label 8900 2300 0    50   ~ 0
~M1
Text Label 8900 2400 0    50   ~ 0
~MREQ
Text Label 8900 2500 0    50   ~ 0
~IORQ
Text Label 8900 2600 0    50   ~ 0
~RD
Text Label 8900 2700 0    50   ~ 0
~WR
Text Label 8900 2800 0    50   ~ 0
~RFSH
Text Label 8900 2900 0    50   ~ 0
~HALT
Text Label 8900 3000 0    50   ~ 0
~WAIT
Text Label 8900 3100 0    50   ~ 0
~INT
Text Label 8900 3200 0    50   ~ 0
~NMI
Text Label 8900 3300 0    50   ~ 0
~RESET
Text Label 8900 3400 0    50   ~ 0
~BUSRQ
Text Label 8900 3500 0    50   ~ 0
~BUSACK
Text Label 8900 3600 0    50   ~ 0
~CLK
Text Label 8400 3100 2    50   ~ 0
IMEM
Text Label 8400 3200 2    50   ~ 0
ICLK
Text Label 8400 3300 2    50   ~ 0
USR0
Text Label 8400 3400 2    50   ~ 0
USR1
Text Label 8400 3500 2    50   ~ 0
USR2
Text Label 8400 3600 2    50   ~ 0
USR3
$Comp
L Connector_Generic:Conn_02x24_Odd_Even ENDOUT1
U 1 1 5F94EC63
P 8600 2500
F 0 "ENDOUT1" H 8650 3817 50  0000 C CNN
F 1 "02x24 female 90deg" H 8650 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x24_P2.54mm_Horizontal" H 8600 2500 50  0001 C CNN
F 3 "~" H 8600 2500 50  0001 C CNN
	1    8600 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3700 3500 3700
Wire Wire Line
	4000 3700 4700 3700
Wire Wire Line
	5200 3700 5850 3700
Wire Wire Line
	6350 3700 6950 3700
Wire Wire Line
	2600 1050 2600 1150
Connection ~ 2600 1150
$Comp
L Device:C_Small C2
U 1 1 5FA30862
P 3750 1050
F 0 "C2" V 3521 1050 50  0000 C CNN
F 1 "100nF" V 3612 1050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3750 1050 50  0001 C CNN
F 3 "~" H 3750 1050 50  0001 C CNN
	1    3750 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 1150 3650 1150
Wire Wire Line
	3650 1150 3650 1050
Wire Wire Line
	3850 1050 4200 1050
$Comp
L Device:C_Small C3
U 1 1 5FA332F9
P 4950 1050
F 0 "C3" V 4721 1050 50  0000 C CNN
F 1 "100nF" V 4812 1050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4950 1050 50  0001 C CNN
F 3 "~" H 4950 1050 50  0001 C CNN
	1    4950 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 1150 4850 1150
Wire Wire Line
	4850 1150 4850 1050
Wire Wire Line
	5050 1050 5400 1050
$Comp
L Device:C_Small C4
U 1 1 5FA36BB1
P 6100 1050
F 0 "C4" V 5871 1050 50  0000 C CNN
F 1 "100nF" V 5962 1050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 6100 1050 50  0001 C CNN
F 3 "~" H 6100 1050 50  0001 C CNN
	1    6100 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 1150 6000 1150
Wire Wire Line
	6000 1150 6000 1050
Wire Wire Line
	6200 1050 6550 1050
$Comp
L Device:C_Small C5
U 1 1 5FA381F9
P 7200 1050
F 0 "C5" V 6971 1050 50  0000 C CNN
F 1 "100nF" V 7062 1050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 7200 1050 50  0001 C CNN
F 3 "~" H 7200 1050 50  0001 C CNN
	1    7200 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 1150 7100 1150
Wire Wire Line
	7100 1150 7100 1050
Wire Wire Line
	7300 1050 7650 1050
Wire Wire Line
	4200 1050 4200 1150
Connection ~ 4200 1150
Wire Wire Line
	5400 1050 5400 1150
Connection ~ 5400 1150
Wire Wire Line
	6550 1050 6550 1150
Connection ~ 6550 1150
Wire Wire Line
	7650 1050 7650 1150
Connection ~ 7650 1150
$Comp
L Device:CP C1
U 1 1 5FA5190A
P 2150 1050
F 0 "C1" V 2405 1050 50  0000 C CNN
F 1 "10uF" V 2314 1050 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2188 900 50  0001 C CNN
F 3 "~" H 2150 1050 50  0001 C CNN
	1    2150 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 1050 2600 1050
Wire Wire Line
	1700 1150 2000 1150
Wire Wire Line
	2000 1150 2000 1050
Connection ~ 1700 1150
Wire Wire Line
	8900 3850 8900 3700
$Comp
L power:GND #PWR?
U 1 1 5FADC8F4
P 1900 3850
F 0 "#PWR?" H 1900 3600 50  0001 C CNN
F 1 "GND" H 1905 3677 50  0000 C CNN
F 2 "" H 1900 3850 50  0001 C CNN
F 3 "" H 1900 3850 50  0001 C CNN
	1    1900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3700 1900 3850
Wire Wire Line
	7450 3850 7450 3700
Wire Wire Line
	7450 3850 8900 3850
$Comp
L power:GND #PWR?
U 1 1 5FAF51CE
P 8400 3950
F 0 "#PWR?" H 8400 3700 50  0001 C CNN
F 1 "GND" H 8405 3777 50  0000 C CNN
F 2 "" H 8400 3950 50  0001 C CNN
F 3 "" H 8400 3950 50  0001 C CNN
	1    8400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3700 8400 3950
$EndSCHEMATC
