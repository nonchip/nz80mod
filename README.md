# NZ80MOD

A modular Z80 computer system.

**Warning**: probably don't build this yet, i haven't tested any of this yet.
I'll remove this warning when i did, and documented everything.


# Modules

## Backplane
![render](backplane/backplane.png)

* supports up to 4 modules
* power/filter caps on board
* all signals parallel, except `IEI`/`IEO` (to allow daisy-chain interrupts)
* daisy-chainable through 90deg headers on the edges

## CPU Board
![render](cpuboard/cpuboard.png)

* Z80 CPU
* 32K ROM (`A15=0`)
* 32K RAM (`A15=1`)
* quartz oscillator (pull `ICLK` low to disable)
* memory mapper (pull `IMEM` low to disable)
* reset button
* NMI button
* pullup resistors for active low cpu control signals
* `IEI NC`, `IEO VCC` (start of daisy chain)

## RAM expansion Board
![render](ramboard/ramboard.png)

**don't use, will be redesigned**

* 2x 32K RAM
* LEDs to show memory selected
* memory mapper
  * address settable via solder jumper (default `0xFF`, cut traces as required)
  * `D0`: lower (`A15=0`) bank enabled
  * `D1`: upper (`A15=1`) bank enabled
  * shadows internal memory (pulls `IMEM` low) during access to enabled bank

## UART Board
![render](uartboard/uartboard.png)

* 2x `SIO` serial controller
  * default address `$84`
  * port A can be mapped to `USR*` and is broken out completely
  * port B has "arduino style ftdi breakout" pin header and can optionally (via jumper) deliver VCC
* `CTC` clock generator
  * default address `$80`
  * counter 0 is broken out on the header with `SIO` port A
  * counter 1 clocks `SIO` port B internally
  * counter 2 triggers through GAL (default on `/M1`)
  * counter 3 triggers on output of counter 2

## Protoboard
![render](protoboard/protoboard.png)

12x24 .1in pitch prototype grid, broken out backplane connector
