EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOTCONN1
U 1 1 5F91CCE1
P 1100 2200
F 0 "SLOTCONN1" H 1150 3517 50  0000 C CNN
F 1 "02x24 male 90deg" H 1150 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Horizontal" H 1100 2200 50  0001 C CNN
F 3 "~" H 1100 2200 50  0001 C CNN
	1    1100 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F921942
P 1600 850
F 0 "#PWR0101" H 1600 600 50  0001 C CNN
F 1 "GND" H 1605 677 50  0000 C CNN
F 2 "" H 1600 850 50  0001 C CNN
F 3 "" H 1600 850 50  0001 C CNN
	1    1600 850 
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F9209DC
P 700 850
F 0 "#PWR0102" H 700 700 50  0001 C CNN
F 1 "+5V" H 715 1023 50  0000 C CNN
F 2 "" H 700 850 50  0001 C CNN
F 3 "" H 700 850 50  0001 C CNN
	1    700  850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  850  700  1100
Wire Wire Line
	1600 850  1600 1100
Wire Wire Line
	1600 1100 1400 1100
Wire Wire Line
	700  1100 900  1100
Text Label 900  1200 2    50   ~ 0
A0
Text Label 900  1300 2    50   ~ 0
A1
Text Label 900  1400 2    50   ~ 0
A2
Text Label 900  1500 2    50   ~ 0
A3
Text Label 900  1600 2    50   ~ 0
A4
Text Label 900  1700 2    50   ~ 0
A5
Text Label 900  1800 2    50   ~ 0
A6
Text Label 900  1900 2    50   ~ 0
A7
Text Label 900  2000 2    50   ~ 0
A8
Text Label 900  2100 2    50   ~ 0
A9
Text Label 900  2200 2    50   ~ 0
A10
Text Label 900  2300 2    50   ~ 0
A11
Text Label 900  2400 2    50   ~ 0
A12
Text Label 900  2500 2    50   ~ 0
A13
Text Label 900  2600 2    50   ~ 0
A14
Text Label 900  2700 2    50   ~ 0
A15
Text Label 1400 1200 0    50   ~ 0
D0
Text Label 1400 1300 0    50   ~ 0
D1
Text Label 1400 1400 0    50   ~ 0
D2
Text Label 1400 1500 0    50   ~ 0
D3
Text Label 1400 1600 0    50   ~ 0
D4
Text Label 1400 1700 0    50   ~ 0
D5
Text Label 1400 1800 0    50   ~ 0
D6
Text Label 1400 1900 0    50   ~ 0
D7
Text Label 1400 2000 0    50   ~ 0
~M1
Text Label 1400 2100 0    50   ~ 0
~MREQ
Text Label 1400 2200 0    50   ~ 0
~IORQ
Text Label 1400 2300 0    50   ~ 0
~RD
Text Label 1400 2400 0    50   ~ 0
~WR
Text Label 1400 2500 0    50   ~ 0
~RFSH
Text Label 1400 2600 0    50   ~ 0
~HALT
Text Label 1400 2700 0    50   ~ 0
~WAIT
Text Label 1400 2800 0    50   ~ 0
~INT
Text Label 1400 2900 0    50   ~ 0
~NMI
Text Label 1400 3000 0    50   ~ 0
~RESET
Text Label 1400 3100 0    50   ~ 0
~BUSRQ
Text Label 1400 3200 0    50   ~ 0
~BUSACK
Text Label 1400 3300 0    50   ~ 0
~CLK
Text Label 900  2800 2    50   ~ 0
IMEM
Text Label 900  2900 2    50   ~ 0
ICLK
Text Label 900  3000 2    50   ~ 0
USR0
Text Label 900  3100 2    50   ~ 0
USR1
Text Label 900  3200 2    50   ~ 0
USR2
Text Label 900  3300 2    50   ~ 0
USR3
Wire Wire Line
	1600 750  1600 850 
Connection ~ 1600 850 
Text Label 900  3400 2    50   ~ 0
IEI
Text Label 1400 3400 0    50   ~ 0
IEO
$Comp
L z84c40:Z84C40-SIO0 U2
U 1 1 5FA544BF
P 5400 2350
F 0 "U2" H 5400 3867 50  0000 C CNN
F 1 "Z84C40-SIO0" H 5400 3776 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm_Socket" H 5400 2350 50  0001 C CNN
F 3 "" H 5400 2350 50  0001 C CNN
	1    5400 2350
	1    0    0    -1  
$EndComp
Text Label 4600 1100 2    50   ~ 0
D0
Text Label 4600 1200 2    50   ~ 0
D1
Text Label 4600 1300 2    50   ~ 0
D2
Text Label 4600 1400 2    50   ~ 0
D3
Text Label 4600 1500 2    50   ~ 0
D4
Text Label 4600 1600 2    50   ~ 0
D5
Text Label 4600 1700 2    50   ~ 0
D6
Text Label 4600 1800 2    50   ~ 0
D7
Text Label 4600 2200 2    50   ~ 0
~RESET
Text Label 4600 2300 2    50   ~ 0
~M1
Text Label 4600 2400 2    50   ~ 0
~IORQ
Text Label 4600 2500 2    50   ~ 0
~RD
Text Label 4600 2800 2    50   ~ 0
A0
Text Label 4600 2900 2    50   ~ 0
A1
Text Label 4600 3600 2    50   ~ 0
IEO
Text Label 4600 3150 2    50   ~ 0
~CLK
Text Label 4600 3400 2    50   ~ 0
~INT
$Comp
L power:GND #PWR09
U 1 1 5FA76A2B
P 8750 2550
F 0 "#PWR09" H 8750 2300 50  0001 C CNN
F 1 "GND" V 8755 2422 50  0000 R CNN
F 2 "" H 8750 2550 50  0001 C CNN
F 3 "" H 8750 2550 50  0001 C CNN
	1    8750 2550
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper JP1
U 1 1 5FA77B71
P 8100 2800
F 0 "JP1" H 8200 2900 50  0000 C CNN
F 1 "PWR-IN" H 8100 2800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8100 2800 50  0001 C CNN
F 3 "~" H 8100 2800 50  0001 C CNN
	1    8100 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2800 8500 2800
Wire Wire Line
	8500 2800 8500 2750
Wire Wire Line
	8500 2750 8750 2750
$Comp
L power:+5V #PWR08
U 1 1 5FA78EB5
P 7800 2800
F 0 "#PWR08" H 7800 2650 50  0001 C CNN
F 1 "+5V" V 7815 2928 50  0000 L CNN
F 2 "" H 7800 2800 50  0001 C CNN
F 3 "" H 7800 2800 50  0001 C CNN
	1    7800 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 2600 6900 2600
Wire Wire Line
	6900 2600 6900 2850
Wire Wire Line
	6900 2850 7400 2850
Wire Wire Line
	6200 2800 6400 2800
Wire Wire Line
	6400 2800 6400 2950
Wire Wire Line
	6400 2950 8750 2950
$Comp
L Z80CTC:Z80CTC IC1
U 1 1 5FA7EDB1
P 5600 5500
F 0 "IC1" H 5550 6670 50  0000 C CNN
F 1 "Z80CTC" H 5550 6579 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 5600 5500 50  0001 L BNN
F 3 "" H 5600 5500 50  0001 C CNN
	1    5600 5500
	1    0    0    -1  
$EndComp
Text Label 4900 4700 2    50   ~ 0
D0
Text Label 4900 4800 2    50   ~ 0
D1
Text Label 4900 4900 2    50   ~ 0
D2
Text Label 4900 5000 2    50   ~ 0
D3
Text Label 4900 5100 2    50   ~ 0
D4
Text Label 4900 5200 2    50   ~ 0
D5
Text Label 4900 5300 2    50   ~ 0
D6
Text Label 4900 5400 2    50   ~ 0
D7
Text Label 4900 5700 2    50   ~ 0
A0
Text Label 4900 5800 2    50   ~ 0
A1
Text Label 4900 5900 2    50   ~ 0
~M1
Text Label 4900 6000 2    50   ~ 0
~IORQ
Text Label 4900 6100 2    50   ~ 0
~RD
Text Label 4900 6300 2    50   ~ 0
IEI
Text Label 4900 6500 2    50   ~ 0
~INT
$Comp
L power:GND #PWR04
U 1 1 5FAA2150
P 5400 3850
F 0 "#PWR04" H 5400 3600 50  0001 C CNN
F 1 "GND" H 5405 3677 50  0000 C CNN
F 2 "" H 5400 3850 50  0001 C CNN
F 3 "" H 5400 3850 50  0001 C CNN
	1    5400 3850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5FAB29BA
P 5400 850
F 0 "#PWR03" H 5400 700 50  0001 C CNN
F 1 "+5V" H 5415 1023 50  0000 C CNN
F 2 "" H 5400 850 50  0001 C CNN
F 3 "" H 5400 850 50  0001 C CNN
	1    5400 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FAB5007
P 6200 6500
F 0 "#PWR06" H 6200 6250 50  0001 C CNN
F 1 "GND" V 6205 6372 50  0000 R CNN
F 2 "" H 6200 6500 50  0001 C CNN
F 3 "" H 6200 6500 50  0001 C CNN
	1    6200 6500
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5FAB628B
P 6200 6300
F 0 "#PWR05" H 6200 6150 50  0001 C CNN
F 1 "+5V" V 6215 6428 50  0000 L CNN
F 2 "" H 6200 6300 50  0001 C CNN
F 3 "" H 6200 6300 50  0001 C CNN
	1    6200 6300
	0    1    1    0   
$EndComp
Text Label 6200 6100 0    50   ~ 0
~CLK
Text Label 6200 5900 0    50   ~ 0
~RESET
Text Label 2250 5350 2    50   ~ 0
~M1
NoConn ~ 6200 5100
Wire Wire Line
	8750 2650 7700 2650
Wire Wire Line
	7300 2650 7300 3200
Wire Wire Line
	7300 3200 6200 3200
$Comp
L Device:R R1
U 1 1 5FAD7B1C
P 7550 2650
F 0 "R1" V 7600 2800 50  0000 C CNN
F 1 "2K2" V 7550 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7480 2650 50  0001 C CNN
F 3 "~" H 7550 2650 50  0001 C CNN
	1    7550 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 2650 7300 2650
$Comp
L Device:R R2
U 1 1 5FAD8099
P 7550 2850
F 0 "R2" V 7600 3000 50  0000 C CNN
F 1 "2K2" V 7550 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7480 2850 50  0001 C CNN
F 3 "~" H 7550 2850 50  0001 C CNN
	1    7550 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	7700 2850 8750 2850
NoConn ~ 6200 3400
NoConn ~ 8750 3050
NoConn ~ 6200 2900
NoConn ~ 6200 3000
$Comp
L power:GND #PWR07
U 1 1 5FAE8CBF
P 6500 3500
F 0 "#PWR07" H 6500 3250 50  0001 C CNN
F 1 "GND" H 6505 3327 50  0000 C CNN
F 2 "" H 6500 3500 50  0001 C CNN
F 3 "" H 6500 3500 50  0001 C CNN
	1    6500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3500 6500 3500
Wire Wire Line
	6200 3300 6500 3300
Wire Wire Line
	6500 3300 6500 3500
Connection ~ 6500 3500
Wire Wire Line
	6200 5200 6650 5200
Wire Wire Line
	6650 5200 6650 2700
Wire Wire Line
	6650 2700 6200 2700
Wire Wire Line
	6200 1200 6900 1200
Wire Wire Line
	6200 1300 6900 1300
Wire Wire Line
	6200 1400 6900 1400
Wire Wire Line
	6200 1500 6900 1500
Wire Wire Line
	6200 1600 6900 1600
Wire Wire Line
	6200 1700 6900 1700
Wire Wire Line
	6200 1900 6400 1900
Wire Wire Line
	6400 1900 6400 1800
Wire Wire Line
	6400 1800 6900 1800
Wire Wire Line
	6200 2000 6450 2000
Wire Wire Line
	6450 2000 6450 1900
Wire Wire Line
	6450 1900 6900 1900
Wire Wire Line
	6200 2100 6500 2100
Wire Wire Line
	6500 2100 6500 2000
Wire Wire Line
	6500 2000 6900 2000
Wire Wire Line
	6200 2200 6550 2200
Wire Wire Line
	6550 2200 6550 2100
Wire Wire Line
	6550 2100 6900 2100
$Comp
L Connector:Conn_01x12_Female J1
U 1 1 5FB20727
P 7100 1700
F 0 "J1" H 7128 1676 50  0000 L CNN
F 1 "A/0 FULL" H 7128 1585 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x12_P2.54mm_Horizontal" H 7100 1700 50  0001 C CNN
F 3 "~" H 7100 1700 50  0001 C CNN
	1    7100 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male J2
U 1 1 5FB232FB
P 8950 2750
F 0 "J2" H 8922 2724 50  0000 R CNN
F 1 "B-FTDI" H 8922 2633 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 8950 2750 50  0001 C CNN
F 3 "~" H 8950 2750 50  0001 C CNN
	1    8950 2750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 4800 6750 4800
Wire Wire Line
	6750 4800 6750 2200
Wire Wire Line
	6750 2200 6900 2200
Wire Wire Line
	6200 4900 6800 4900
Wire Wire Line
	6800 4900 6800 2300
Wire Wire Line
	6800 2300 6900 2300
Text Label 4600 2100 2    50   ~ 0
~CE-SIO
Text Label 4900 5600 2    50   ~ 0
~CE-CTC
Text Label 3100 5350 0    50   ~ 0
~CE-SIO
Text Label 3100 5450 0    50   ~ 0
~CE-CTC
Text Label 2250 6050 2    50   ~ 0
~IORQ
Text Label 2250 5950 2    50   ~ 0
A7
Text Label 2250 5850 2    50   ~ 0
A6
Text Label 2250 5750 2    50   ~ 0
A5
Text Label 2250 5650 2    50   ~ 0
A4
Text Label 2250 5550 2    50   ~ 0
A3
Text Label 2250 5450 2    50   ~ 0
A2
$Comp
L power:+5V #PWR02
U 1 1 5FB52337
P 3100 5250
F 0 "#PWR02" H 3100 5100 50  0001 C CNN
F 1 "+5V" H 3115 5423 50  0000 C CNN
F 2 "" H 3100 5250 50  0001 C CNN
F 3 "" H 3100 5250 50  0001 C CNN
	1    3100 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FB514C2
P 2250 6350
F 0 "#PWR01" H 2250 6100 50  0001 C CNN
F 1 "GND" H 2255 6177 50  0000 C CNN
F 2 "" H 2250 6350 50  0001 C CNN
F 3 "" H 2250 6350 50  0001 C CNN
	1    2250 6350
	1    0    0    -1  
$EndComp
$Comp
L symbols:ATF22V10 U1
U 1 1 5FB508CA
P 2350 5150
F 0 "U1" H 2675 5315 50  0000 C CNN
F 1 "ATF22V10" H 2675 5224 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_Socket" H 2350 5150 50  0001 C CNN
F 3 "" H 2350 5150 50  0001 C CNN
	1    2350 5150
	1    0    0    -1  
$EndComp
NoConn ~ 2250 5250
NoConn ~ 2250 6150
NoConn ~ 3100 5750
NoConn ~ 3100 5650
Text Label 6200 1200 0    50   ~ 0
RXA
Text Label 6200 1400 0    50   ~ 0
TXA
Text Label 6200 4900 0    50   ~ 0
TO0
Text Label 3100 6350 0    50   ~ 0
TO0
Text Label 2250 6250 2    50   ~ 0
TXA
Text Label 3100 6250 0    50   ~ 0
RXA
Text Label 3100 6150 0    50   ~ 0
USR0
Text Label 3100 6050 0    50   ~ 0
USR1
Text Label 3100 5950 0    50   ~ 0
USR2
Text Label 3100 5850 0    50   ~ 0
USR3
$Comp
L Device:C C1
U 1 1 5FA1ECA1
P 8000 4400
F 0 "C1" H 8115 4446 50  0000 L CNN
F 1 "C" H 8115 4355 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8038 4250 50  0001 C CNN
F 3 "~" H 8000 4400 50  0001 C CNN
	1    8000 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FA1F7F6
P 8300 4400
F 0 "C2" H 8415 4446 50  0000 L CNN
F 1 "C" H 8415 4355 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8338 4250 50  0001 C CNN
F 3 "~" H 8300 4400 50  0001 C CNN
	1    8300 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FA1FD43
P 8600 4400
F 0 "C3" H 8715 4446 50  0000 L CNN
F 1 "C" H 8715 4355 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8638 4250 50  0001 C CNN
F 3 "~" H 8600 4400 50  0001 C CNN
	1    8600 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5FA201E0
P 8300 4550
F 0 "#PWR011" H 8300 4300 50  0001 C CNN
F 1 "GND" V 8305 4422 50  0000 R CNN
F 2 "" H 8300 4550 50  0001 C CNN
F 3 "" H 8300 4550 50  0001 C CNN
	1    8300 4550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5FA218B4
P 8300 4250
F 0 "#PWR010" H 8300 4100 50  0001 C CNN
F 1 "+5V" V 8315 4378 50  0000 L CNN
F 2 "" H 8300 4250 50  0001 C CNN
F 3 "" H 8300 4250 50  0001 C CNN
	1    8300 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4250 8300 4250
Connection ~ 8300 4250
Wire Wire Line
	8300 4250 8600 4250
Wire Wire Line
	8600 4550 8300 4550
Connection ~ 8300 4550
Wire Wire Line
	8000 4550 8300 4550
Wire Wire Line
	4900 6400 4250 6400
Wire Wire Line
	4250 6400 4250 3500
Wire Wire Line
	4250 3500 4600 3500
Wire Wire Line
	6200 5500 6200 5700
Wire Wire Line
	3100 5550 4100 5550
Wire Wire Line
	4100 5550 4100 6650
Wire Wire Line
	4100 6650 6550 6650
Wire Wire Line
	6550 6650 6550 5400
Wire Wire Line
	6550 5400 6200 5400
$EndSCHEMATC
