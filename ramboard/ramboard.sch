EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOTCONN1
U 1 1 5F91CCE1
P 1650 2350
F 0 "SLOTCONN1" H 1700 3667 50  0000 C CNN
F 1 "02x24 male 90deg" H 1700 3576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Horizontal" H 1650 2350 50  0001 C CNN
F 3 "~" H 1650 2350 50  0001 C CNN
	1    1650 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F921942
P 2150 900
F 0 "#PWR0101" H 2150 650 50  0001 C CNN
F 1 "GND" H 2155 727 50  0000 C CNN
F 2 "" H 2150 900 50  0001 C CNN
F 3 "" H 2150 900 50  0001 C CNN
	1    2150 900 
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F9209DC
P 1250 1000
F 0 "#PWR0102" H 1250 850 50  0001 C CNN
F 1 "+5V" H 1265 1173 50  0000 C CNN
F 2 "" H 1250 1000 50  0001 C CNN
F 3 "" H 1250 1000 50  0001 C CNN
	1    1250 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1000 1250 1250
Wire Wire Line
	2150 1250 1950 1250
Wire Wire Line
	1250 1250 1450 1250
Text Label 1450 1350 2    50   ~ 0
A0
Text Label 1450 1450 2    50   ~ 0
A1
Text Label 1450 1550 2    50   ~ 0
A2
Text Label 1450 1650 2    50   ~ 0
A3
Text Label 1450 1750 2    50   ~ 0
A4
Text Label 1450 1850 2    50   ~ 0
A5
Text Label 1450 1950 2    50   ~ 0
A6
Text Label 1450 2050 2    50   ~ 0
A7
Text Label 1450 2850 2    50   ~ 0
A15
Text Label 1950 1350 0    50   ~ 0
D0
Text Label 1950 1450 0    50   ~ 0
D1
Text Label 1950 2150 0    50   ~ 0
~M1
Text Label 1950 2250 0    50   ~ 0
~MREQ
Text Label 1950 2350 0    50   ~ 0
~IORQ
Text Label 1950 2450 0    50   ~ 0
~RD
Text Label 1950 2550 0    50   ~ 0
~WR
Text Label 1450 2950 2    50   ~ 0
IMEM
$Comp
L symbols:ATF22V10 U1
U 1 1 5F9632F2
P 1400 4250
F 0 "U1" H 1725 4415 50  0000 C CNN
F 1 "ATF22V10" H 1725 4324 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_Socket" H 1400 4250 50  0001 C CNN
F 3 "" H 1400 4250 50  0001 C CNN
	1    1400 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F982344
P 2150 4200
F 0 "#PWR0104" H 2150 4050 50  0001 C CNN
F 1 "+5V" H 2165 4373 50  0000 C CNN
F 2 "" H 2150 4200 50  0001 C CNN
F 3 "" H 2150 4200 50  0001 C CNN
	1    2150 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5F982DDD
P 1300 5600
F 0 "#PWR0105" H 1300 5350 50  0001 C CNN
F 1 "GND" H 1305 5427 50  0000 C CNN
F 2 "" H 1300 5600 50  0001 C CNN
F 3 "" H 1300 5600 50  0001 C CNN
	1    1300 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5450 1300 5600
Wire Wire Line
	2150 4200 2150 4350
$Comp
L Memory_RAM:HM62256BLP U3
U 1 1 5F9AD66E
P 6200 2150
F 0 "U3" H 6200 3231 50  0000 C CNN
F 1 "HM62256BLP" H 6200 3140 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 6200 2050 50  0001 C CNN
F 3 "https://web.mit.edu/6.115/www/document/62256.pdf" H 6200 2050 50  0001 C CNN
	1    6200 2150
	1    0    0    -1  
$EndComp
Text Label 5700 1450 2    50   ~ 0
A0
Text Label 5700 1550 2    50   ~ 0
A1
Text Label 5700 1650 2    50   ~ 0
A2
Text Label 5700 1750 2    50   ~ 0
A3
Text Label 5700 1850 2    50   ~ 0
A4
Text Label 5700 1950 2    50   ~ 0
A5
Text Label 5700 2050 2    50   ~ 0
A6
Text Label 5700 2150 2    50   ~ 0
A7
Text Label 5700 2250 2    50   ~ 0
A8
Text Label 5700 2350 2    50   ~ 0
A9
Text Label 5700 2450 2    50   ~ 0
A10
Text Label 5700 2550 2    50   ~ 0
A11
Text Label 5700 2650 2    50   ~ 0
A12
Text Label 5700 2750 2    50   ~ 0
A13
Text Label 5700 2850 2    50   ~ 0
A14
Text Label 6700 1450 0    50   ~ 0
D0
Text Label 6700 1550 0    50   ~ 0
D1
Text Label 6700 1650 0    50   ~ 0
D2
Text Label 6700 1750 0    50   ~ 0
D3
Text Label 6700 1850 0    50   ~ 0
D4
Text Label 6700 1950 0    50   ~ 0
D5
Text Label 6700 2050 0    50   ~ 0
D6
Text Label 6700 2150 0    50   ~ 0
D7
Text Label 6700 2550 0    50   ~ 0
~RD
Text Label 6700 2650 0    50   ~ 0
~WR
$Comp
L power:+5V #PWR0111
U 1 1 5F9BE0F6
P 6200 1050
F 0 "#PWR0111" H 6200 900 50  0001 C CNN
F 1 "+5V" H 6215 1223 50  0000 C CNN
F 2 "" H 6200 1050 50  0001 C CNN
F 3 "" H 6200 1050 50  0001 C CNN
	1    6200 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5F9BE82C
P 6200 3250
F 0 "#PWR0112" H 6200 3000 50  0001 C CNN
F 1 "GND" H 6205 3077 50  0000 C CNN
F 2 "" H 6200 3250 50  0001 C CNN
F 3 "" H 6200 3250 50  0001 C CNN
	1    6200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3250 6200 3050
Wire Wire Line
	6200 1050 6200 1250
Wire Wire Line
	2150 900  2150 1250
$Comp
L power:+5V #PWR0119
U 1 1 5FCCEE24
P 1050 6750
F 0 "#PWR0119" H 1050 6600 50  0001 C CNN
F 1 "+5V" H 1065 6923 50  0000 C CNN
F 2 "" H 1050 6750 50  0001 C CNN
F 3 "" H 1050 6750 50  0001 C CNN
	1    1050 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5FCD1A78
P 1050 7050
F 0 "#PWR0120" H 1050 6800 50  0001 C CNN
F 1 "GND" H 1055 6877 50  0000 C CNN
F 2 "" H 1050 7050 50  0001 C CNN
F 3 "" H 1050 7050 50  0001 C CNN
	1    1050 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FCD8193
P 1350 6900
F 0 "C1" H 1465 6946 50  0000 L CNN
F 1 "C" H 1465 6855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1388 6750 50  0001 C CNN
F 3 "~" H 1350 6900 50  0001 C CNN
	1    1350 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FCD89CB
P 1650 6900
F 0 "C2" H 1765 6946 50  0000 L CNN
F 1 "C" H 1765 6855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1688 6750 50  0001 C CNN
F 3 "~" H 1650 6900 50  0001 C CNN
	1    1650 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FCD9093
P 1900 6900
F 0 "C3" H 2015 6946 50  0000 L CNN
F 1 "C" H 2015 6855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1938 6750 50  0001 C CNN
F 3 "~" H 1900 6900 50  0001 C CNN
	1    1900 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 6750 1350 6750
Wire Wire Line
	1350 6750 1650 6750
Connection ~ 1350 6750
Wire Wire Line
	1650 6750 1900 6750
Connection ~ 1650 6750
Wire Wire Line
	1900 7050 1650 7050
Wire Wire Line
	1650 7050 1350 7050
Connection ~ 1650 7050
Wire Wire Line
	1050 7050 1350 7050
Connection ~ 1350 7050
$Comp
L Memory_RAM:HM62256BLP U2
U 1 1 5FD8529D
P 4400 2150
F 0 "U2" H 4400 3231 50  0000 C CNN
F 1 "HM62256BLP" H 4400 3140 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 4400 2050 50  0001 C CNN
F 3 "https://web.mit.edu/6.115/www/document/62256.pdf" H 4400 2050 50  0001 C CNN
	1    4400 2150
	1    0    0    -1  
$EndComp
Text Label 3900 1450 2    50   ~ 0
A0
Text Label 3900 1550 2    50   ~ 0
A1
Text Label 3900 1650 2    50   ~ 0
A2
Text Label 3900 1750 2    50   ~ 0
A3
Text Label 3900 1850 2    50   ~ 0
A4
Text Label 3900 1950 2    50   ~ 0
A5
Text Label 3900 2050 2    50   ~ 0
A6
Text Label 3900 2150 2    50   ~ 0
A7
Text Label 3900 2250 2    50   ~ 0
A8
Text Label 3900 2350 2    50   ~ 0
A9
Text Label 3900 2450 2    50   ~ 0
A10
Text Label 3900 2550 2    50   ~ 0
A11
Text Label 3900 2650 2    50   ~ 0
A12
Text Label 3900 2750 2    50   ~ 0
A13
Text Label 3900 2850 2    50   ~ 0
A14
Text Label 4900 1450 0    50   ~ 0
D0
Text Label 4900 1550 0    50   ~ 0
D1
Text Label 4900 1650 0    50   ~ 0
D2
Text Label 4900 1750 0    50   ~ 0
D3
Text Label 4900 1850 0    50   ~ 0
D4
Text Label 4900 1950 0    50   ~ 0
D5
Text Label 4900 2050 0    50   ~ 0
D6
Text Label 4900 2150 0    50   ~ 0
D7
Text Label 4900 2550 0    50   ~ 0
~RD
Text Label 4900 2650 0    50   ~ 0
~WR
$Comp
L power:+5V #PWR0106
U 1 1 5FD852BC
P 4400 1050
F 0 "#PWR0106" H 4400 900 50  0001 C CNN
F 1 "+5V" H 4415 1223 50  0000 C CNN
F 2 "" H 4400 1050 50  0001 C CNN
F 3 "" H 4400 1050 50  0001 C CNN
	1    4400 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5FD852C2
P 4400 3250
F 0 "#PWR0107" H 4400 3000 50  0001 C CNN
F 1 "GND" H 4405 3077 50  0000 C CNN
F 2 "" H 4400 3250 50  0001 C CNN
F 3 "" H 4400 3250 50  0001 C CNN
	1    4400 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3250 4400 3050
Wire Wire Line
	4400 1050 4400 1250
Text Label 1300 5150 2    50   ~ 0
A15
Text Label 2150 4450 0    50   ~ 0
IMEM
Text Label 2150 5450 0    50   ~ 0
~M1
Text Label 1300 5050 2    50   ~ 0
~MREQ
Text Label 1300 5350 2    50   ~ 0
~WR
Text Label 2150 5350 0    50   ~ 0
D0
Wire Wire Line
	4900 2350 5100 2350
Wire Wire Line
	5100 2350 5100 4550
Wire Wire Line
	5100 4550 2150 4550
Wire Wire Line
	6700 2350 6800 2350
Wire Wire Line
	6800 2350 6800 4650
Wire Wire Line
	6800 4650 2150 4650
$Comp
L Device:LED D1
U 1 1 5FE00245
P 2850 4150
F 0 "D1" V 2889 4032 50  0000 R CNN
F 1 "LED" V 2798 4032 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 2850 4150 50  0001 C CNN
F 3 "~" H 2850 4150 50  0001 C CNN
	1    2850 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5FE05611
P 3200 4150
F 0 "D2" V 3239 4032 50  0000 R CNN
F 1 "LED" V 3148 4032 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 3200 4150 50  0001 C CNN
F 3 "~" H 3200 4150 50  0001 C CNN
	1    3200 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2150 4750 2850 4750
Wire Wire Line
	2850 4750 2850 4300
Wire Wire Line
	2150 4850 3200 4850
Wire Wire Line
	3200 4850 3200 4300
Text Label 1950 2050 0    50   ~ 0
D7
Text Label 1950 1950 0    50   ~ 0
D6
Text Label 1950 1850 0    50   ~ 0
D5
Text Label 1950 1750 0    50   ~ 0
D4
Text Label 1950 1650 0    50   ~ 0
D3
Text Label 1950 1550 0    50   ~ 0
D2
Text Label 1450 2750 2    50   ~ 0
A14
Text Label 1450 2650 2    50   ~ 0
A13
Text Label 1450 2550 2    50   ~ 0
A12
Text Label 1450 2450 2    50   ~ 0
A11
Text Label 1450 2350 2    50   ~ 0
A10
Text Label 1450 2250 2    50   ~ 0
A9
Text Label 1450 2150 2    50   ~ 0
A8
NoConn ~ 1450 3050
NoConn ~ 1450 3150
NoConn ~ 1450 3250
NoConn ~ 1450 3350
NoConn ~ 1450 3450
NoConn ~ 1950 3350
NoConn ~ 1950 3250
NoConn ~ 1950 3050
NoConn ~ 1950 2950
NoConn ~ 1950 2850
NoConn ~ 1950 2750
NoConn ~ 1950 2650
$Comp
L power:+5V #PWR0108
U 1 1 5FE8710D
P 2850 3550
F 0 "#PWR0108" H 2850 3400 50  0001 C CNN
F 1 "+5V" H 2865 3723 50  0000 C CNN
F 2 "" H 2850 3550 50  0001 C CNN
F 3 "" H 2850 3550 50  0001 C CNN
	1    2850 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FE87AA7
P 2850 3850
F 0 "R1" H 2920 3896 50  0000 L CNN
F 1 "330R" H 2920 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2780 3850 50  0001 C CNN
F 3 "~" H 2850 3850 50  0001 C CNN
	1    2850 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5FE88788
P 3200 3850
F 0 "R2" H 3270 3896 50  0000 L CNN
F 1 "330R" H 3270 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3130 3850 50  0001 C CNN
F 3 "~" H 3200 3850 50  0001 C CNN
	1    3200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3550 2850 3700
Wire Wire Line
	2850 3550 3200 3550
Wire Wire Line
	3200 3550 3200 3700
Connection ~ 2850 3550
NoConn ~ 2150 4950
NoConn ~ 2150 5050
Text Label 1300 5250 2    50   ~ 0
~RD
Wire Wire Line
	1450 3550 1450 3700
Wire Wire Line
	1450 3700 1950 3700
Wire Wire Line
	1950 3700 1950 3550
Text Label 1950 3150 0    50   ~ 0
~RESET
Text Label 1300 4950 2    50   ~ 0
~RESET
$Comp
L 74xx:74HC688 U4
U 1 1 5F99B425
P 5850 6100
F 0 "U4" H 6394 6146 50  0000 L CNN
F 1 "74HC688" H 6394 6055 50  0000 L CNN
F 2 "Package_DIP:DIP-20_W7.62mm_Socket" H 5850 6100 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/cd54hc688.pdf" H 5850 6100 50  0001 C CNN
	1    5850 6100
	1    0    0    -1  
$EndComp
Text Label 5350 5200 2    50   ~ 0
A0
Text Label 5350 5300 2    50   ~ 0
A1
Text Label 5350 5400 2    50   ~ 0
A2
Text Label 5350 5500 2    50   ~ 0
A3
Text Label 5350 5600 2    50   ~ 0
A4
Text Label 5350 5700 2    50   ~ 0
A5
Text Label 5350 5800 2    50   ~ 0
A6
Text Label 5350 5900 2    50   ~ 0
A7
Text Label 1300 4850 2    50   ~ 0
~IORQ
Text Label 1950 3450 0    50   ~ 0
~CLK
Text Label 1300 4350 2    50   ~ 0
~CLK
Text Label 6350 5200 0    50   ~ 0
~Addressed
Text Label 1300 4450 2    50   ~ 0
~Addressed
Wire Wire Line
	5350 7000 5350 7300
Wire Wire Line
	5350 7300 5550 7300
Text Label 2150 5250 0    50   ~ 0
D1
$Comp
L Jumper:SolderJumper_3_Bridged12 JP0
U 1 1 5F9EC52A
P 4650 5300
F 0 "JP0" H 4650 5505 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 4850 5700 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 4650 5300 50  0001 C CNN
F 3 "~" H 4650 5300 50  0001 C CNN
	1    4650 5300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP1
U 1 1 5F9FB50F
P 4200 5300
F 0 "JP1" H 4200 5505 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 4300 5600 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 4200 5300 50  0001 C CNN
F 3 "~" H 4200 5300 50  0001 C CNN
	1    4200 5300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP2
U 1 1 5F9FBF2E
P 3750 5300
F 0 "JP2" H 3750 5505 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 3850 5700 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 3750 5300 50  0001 C CNN
F 3 "~" H 3750 5300 50  0001 C CNN
	1    3750 5300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP3
U 1 1 5F9FC834
P 3300 5300
F 0 "JP3" H 3300 5505 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 3300 5600 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 3300 5300 50  0001 C CNN
F 3 "~" H 3300 5300 50  0001 C CNN
	1    3300 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5450 4650 6100
Wire Wire Line
	4650 6100 5350 6100
Wire Wire Line
	4200 5450 4200 6200
Wire Wire Line
	4200 6200 5350 6200
Wire Wire Line
	3750 5450 3750 6300
Wire Wire Line
	3750 6300 5350 6300
Wire Wire Line
	3300 5450 3300 6400
Wire Wire Line
	3300 6400 5350 6400
$Comp
L Jumper:SolderJumper_3_Bridged12 JP4
U 1 1 5FA05AD9
P 3250 7200
F 0 "JP4" H 3250 7313 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 3650 7550 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 3250 7200 50  0001 C CNN
F 3 "~" H 3250 7200 50  0001 C CNN
	1    3250 7200
	-1   0    0    1   
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP5
U 1 1 5FA05ADF
P 3700 7200
F 0 "JP5" H 3700 7405 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 3900 7500 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 3700 7200 50  0001 C CNN
F 3 "~" H 3700 7200 50  0001 C CNN
	1    3700 7200
	-1   0    0    1   
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP6
U 1 1 5FA05AE5
P 4150 7200
F 0 "JP6" H 4150 7405 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 4350 7550 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 4150 7200 50  0001 C CNN
F 3 "~" H 4150 7200 50  0001 C CNN
	1    4150 7200
	-1   0    0    1   
$EndComp
$Comp
L Jumper:SolderJumper_3_Bridged12 JP7
U 1 1 5FA05AEB
P 4600 7200
F 0 "JP7" H 4600 7405 50  0000 C CNN
F 1 "SolderJumper_3_Bridged12" H 4600 7500 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 4600 7200 50  0001 C CNN
F 3 "~" H 4600 7200 50  0001 C CNN
	1    4600 7200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 7050 4600 6800
Wire Wire Line
	4600 6800 5350 6800
Wire Wire Line
	5350 6700 4150 6700
Wire Wire Line
	4150 6700 4150 7050
Wire Wire Line
	5350 6600 3700 6600
Wire Wire Line
	3700 6600 3700 7050
Wire Wire Line
	5350 6500 3250 6500
Wire Wire Line
	3250 6500 3250 7050
$Comp
L power:GND #PWR0103
U 1 1 5FA10C05
P 4900 5100
F 0 "#PWR0103" H 4900 4850 50  0001 C CNN
F 1 "GND" V 4905 4972 50  0000 R CNN
F 2 "" H 4900 5100 50  0001 C CNN
F 3 "" H 4900 5100 50  0001 C CNN
	1    4900 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 5300 4850 5100
Wire Wire Line
	4850 5100 4900 5100
Wire Wire Line
	4850 5100 4400 5100
Wire Wire Line
	4400 5100 4400 5300
Connection ~ 4850 5100
Wire Wire Line
	3950 5100 3950 5300
Wire Wire Line
	3950 5100 3500 5100
Wire Wire Line
	3500 5100 3500 5300
Connection ~ 3950 5100
Wire Wire Line
	3950 5100 4400 5100
Connection ~ 4400 5100
$Comp
L power:+5V #PWR0109
U 1 1 5FA18D52
P 2800 5200
F 0 "#PWR0109" H 2800 5050 50  0001 C CNN
F 1 "+5V" V 2815 5328 50  0000 L CNN
F 2 "" H 2800 5200 50  0001 C CNN
F 3 "" H 2800 5200 50  0001 C CNN
	1    2800 5200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2800 5200 3100 5200
Wire Wire Line
	3100 5200 3100 5300
Wire Wire Line
	3100 5200 3550 5200
Wire Wire Line
	3550 5200 3550 5300
Connection ~ 3100 5200
Wire Wire Line
	3550 5200 4000 5200
Wire Wire Line
	4000 5200 4000 5300
Connection ~ 3550 5200
Wire Wire Line
	4000 5200 4450 5200
Wire Wire Line
	4450 5200 4450 5300
Connection ~ 4000 5200
$Comp
L power:GND #PWR0110
U 1 1 5FA204FD
P 2950 7400
F 0 "#PWR0110" H 2950 7150 50  0001 C CNN
F 1 "GND" V 2955 7272 50  0000 R CNN
F 2 "" H 2950 7400 50  0001 C CNN
F 3 "" H 2950 7400 50  0001 C CNN
	1    2950 7400
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 5FA21011
P 4900 7300
F 0 "#PWR0113" H 4900 7150 50  0001 C CNN
F 1 "+5V" V 4915 7428 50  0000 L CNN
F 2 "" H 4900 7300 50  0001 C CNN
F 3 "" H 4900 7300 50  0001 C CNN
	1    4900 7300
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 7400 3050 7400
Wire Wire Line
	3050 7400 3050 7200
Wire Wire Line
	3050 7400 3500 7400
Wire Wire Line
	3500 7400 3500 7200
Connection ~ 3050 7400
Wire Wire Line
	3500 7400 3950 7400
Wire Wire Line
	3950 7400 3950 7200
Connection ~ 3500 7400
Wire Wire Line
	3950 7400 4400 7400
Wire Wire Line
	4400 7400 4400 7200
Connection ~ 3950 7400
Wire Wire Line
	4900 7300 4800 7300
Wire Wire Line
	4800 7300 4800 7200
Wire Wire Line
	4800 7300 4350 7300
Wire Wire Line
	4350 7300 4350 7200
Connection ~ 4800 7300
Wire Wire Line
	4350 7300 3900 7300
Wire Wire Line
	3900 7300 3900 7200
Connection ~ 4350 7300
Wire Wire Line
	3900 7300 3450 7300
Wire Wire Line
	3450 7300 3450 7200
Connection ~ 3900 7300
$Comp
L power:+5V #PWR0114
U 1 1 5FA3318C
P 5850 4900
F 0 "#PWR0114" H 5850 4750 50  0001 C CNN
F 1 "+5V" H 5865 5073 50  0000 C CNN
F 2 "" H 5850 4900 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5FA34DDD
P 5550 7300
F 0 "#PWR0115" H 5550 7050 50  0001 C CNN
F 1 "GND" H 5555 7127 50  0000 C CNN
F 2 "" H 5550 7300 50  0001 C CNN
F 3 "" H 5550 7300 50  0001 C CNN
	1    5550 7300
	1    0    0    -1  
$EndComp
Connection ~ 5550 7300
Wire Wire Line
	5550 7300 5850 7300
NoConn ~ 1300 4550
NoConn ~ 1300 4650
NoConn ~ 1300 4750
NoConn ~ 2150 5150
$EndSCHEMATC
