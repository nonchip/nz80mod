EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x24_Odd_Even SLOTCONN1
U 1 1 5F91CCE1
P 3700 2500
F 0 "SLOTCONN1" H 3750 3817 50  0000 C CNN
F 1 "02x24 male 90deg" H 3750 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Horizontal" H 3700 2500 50  0001 C CNN
F 3 "~" H 3700 2500 50  0001 C CNN
	1    3700 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F921942
P 4200 1150
F 0 "#PWR0101" H 4200 900 50  0001 C CNN
F 1 "GND" H 4205 977 50  0000 C CNN
F 2 "" H 4200 1150 50  0001 C CNN
F 3 "" H 4200 1150 50  0001 C CNN
	1    4200 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F9209DC
P 3300 1150
F 0 "#PWR0102" H 3300 1000 50  0001 C CNN
F 1 "+5V" H 3315 1323 50  0000 C CNN
F 2 "" H 3300 1150 50  0001 C CNN
F 3 "" H 3300 1150 50  0001 C CNN
	1    3300 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1150 3300 1400
Wire Wire Line
	4200 1150 4200 1400
Wire Wire Line
	4200 1400 4000 1400
Wire Wire Line
	3300 1400 3500 1400
Text Label 3500 1500 2    50   ~ 0
A0
Text Label 3500 1600 2    50   ~ 0
A1
Text Label 3500 1700 2    50   ~ 0
A2
Text Label 3500 1800 2    50   ~ 0
A3
Text Label 3500 1900 2    50   ~ 0
A4
Text Label 3500 2000 2    50   ~ 0
A5
Text Label 3500 2100 2    50   ~ 0
A6
Text Label 3500 2200 2    50   ~ 0
A7
Text Label 3500 2300 2    50   ~ 0
A8
Text Label 3500 2400 2    50   ~ 0
A9
Text Label 3500 2500 2    50   ~ 0
A10
Text Label 3500 2600 2    50   ~ 0
A11
Text Label 3500 2700 2    50   ~ 0
A12
Text Label 3500 2800 2    50   ~ 0
A13
Text Label 3500 2900 2    50   ~ 0
A14
Text Label 3500 3000 2    50   ~ 0
A15
Text Label 4000 1500 0    50   ~ 0
D0
Text Label 4000 1600 0    50   ~ 0
D1
Text Label 4000 1700 0    50   ~ 0
D2
Text Label 4000 1800 0    50   ~ 0
D3
Text Label 4000 1900 0    50   ~ 0
D4
Text Label 4000 2000 0    50   ~ 0
D5
Text Label 4000 2100 0    50   ~ 0
D6
Text Label 4000 2200 0    50   ~ 0
D7
Text Label 4000 2300 0    50   ~ 0
~M1
Text Label 4000 2400 0    50   ~ 0
~MREQ
Text Label 4000 2500 0    50   ~ 0
~IORQ
Text Label 4000 2600 0    50   ~ 0
~RD
Text Label 4000 2700 0    50   ~ 0
~WR
Text Label 4000 2800 0    50   ~ 0
~RFSH
Text Label 4000 2900 0    50   ~ 0
~HALT
Text Label 4000 3000 0    50   ~ 0
~WAIT
Text Label 4000 3100 0    50   ~ 0
~INT
Text Label 4000 3200 0    50   ~ 0
~NMI
Text Label 4000 3300 0    50   ~ 0
~RESET
Text Label 4000 3400 0    50   ~ 0
~BUSRQ
Text Label 4000 3500 0    50   ~ 0
~BUSACK
Text Label 4000 3600 0    50   ~ 0
~CLK
Text Label 3500 3100 2    50   ~ 0
IMEM
Text Label 3500 3200 2    50   ~ 0
ICLK
Text Label 3500 3300 2    50   ~ 0
USR0
Text Label 3500 3400 2    50   ~ 0
USR1
Text Label 3500 3500 2    50   ~ 0
USR2
Text Label 3500 3600 2    50   ~ 0
USR3
Wire Wire Line
	4200 1050 4200 1150
Connection ~ 4200 1150
Text Label 3500 3700 2    50   ~ 0
IEI
Text Label 4000 3700 0    50   ~ 0
IEO
$Comp
L Connector_Generic:Conn_02x24_Odd_Even slotholes1
U 1 1 5F95A0A6
P 5250 2550
F 0 "slotholes1" H 5300 3867 50  0000 C CNN
F 1 "02x24 holes" H 5300 3776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x24_P2.54mm_Vertical" H 5250 2550 50  0001 C CNN
F 3 "~" H 5250 2550 50  0001 C CNN
	1    5250 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5F95A0AC
P 5750 1200
F 0 "#PWR0103" H 5750 950 50  0001 C CNN
F 1 "GND" H 5755 1027 50  0000 C CNN
F 2 "" H 5750 1200 50  0001 C CNN
F 3 "" H 5750 1200 50  0001 C CNN
	1    5750 1200
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F95A0B2
P 4850 1200
F 0 "#PWR0104" H 4850 1050 50  0001 C CNN
F 1 "+5V" H 4865 1373 50  0000 C CNN
F 2 "" H 4850 1200 50  0001 C CNN
F 3 "" H 4850 1200 50  0001 C CNN
	1    4850 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1200 4850 1450
Wire Wire Line
	5750 1200 5750 1450
Wire Wire Line
	5750 1450 5550 1450
Wire Wire Line
	4850 1450 5050 1450
Text Label 5050 1550 2    50   ~ 0
A0
Text Label 5050 1650 2    50   ~ 0
A1
Text Label 5050 1750 2    50   ~ 0
A2
Text Label 5050 1850 2    50   ~ 0
A3
Text Label 5050 1950 2    50   ~ 0
A4
Text Label 5050 2050 2    50   ~ 0
A5
Text Label 5050 2150 2    50   ~ 0
A6
Text Label 5050 2250 2    50   ~ 0
A7
Text Label 5050 2350 2    50   ~ 0
A8
Text Label 5050 2450 2    50   ~ 0
A9
Text Label 5050 2550 2    50   ~ 0
A10
Text Label 5050 2650 2    50   ~ 0
A11
Text Label 5050 2750 2    50   ~ 0
A12
Text Label 5050 2850 2    50   ~ 0
A13
Text Label 5050 2950 2    50   ~ 0
A14
Text Label 5050 3050 2    50   ~ 0
A15
Text Label 5550 1550 0    50   ~ 0
D0
Text Label 5550 1650 0    50   ~ 0
D1
Text Label 5550 1750 0    50   ~ 0
D2
Text Label 5550 1850 0    50   ~ 0
D3
Text Label 5550 1950 0    50   ~ 0
D4
Text Label 5550 2050 0    50   ~ 0
D5
Text Label 5550 2150 0    50   ~ 0
D6
Text Label 5550 2250 0    50   ~ 0
D7
Text Label 5550 2350 0    50   ~ 0
~M1
Text Label 5550 2450 0    50   ~ 0
~MREQ
Text Label 5550 2550 0    50   ~ 0
~IORQ
Text Label 5550 2650 0    50   ~ 0
~RD
Text Label 5550 2750 0    50   ~ 0
~WR
Text Label 5550 2850 0    50   ~ 0
~RFSH
Text Label 5550 2950 0    50   ~ 0
~HALT
Text Label 5550 3050 0    50   ~ 0
~WAIT
Text Label 5550 3150 0    50   ~ 0
~INT
Text Label 5550 3250 0    50   ~ 0
~NMI
Text Label 5550 3350 0    50   ~ 0
~RESET
Text Label 5550 3450 0    50   ~ 0
~BUSRQ
Text Label 5550 3550 0    50   ~ 0
~BUSACK
Text Label 5550 3650 0    50   ~ 0
~CLK
Text Label 5050 3150 2    50   ~ 0
IMEM
Text Label 5050 3250 2    50   ~ 0
ICLK
Text Label 5050 3350 2    50   ~ 0
USR0
Text Label 5050 3450 2    50   ~ 0
USR1
Text Label 5050 3550 2    50   ~ 0
USR2
Text Label 5050 3650 2    50   ~ 0
USR3
Wire Wire Line
	5750 1100 5750 1200
Connection ~ 5750 1200
Text Label 5050 3750 2    50   ~ 0
IEI
Text Label 5550 3750 0    50   ~ 0
IEO
$EndSCHEMATC
